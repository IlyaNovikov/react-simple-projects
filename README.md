## Five simple react projects

Each project is in a separate branch

### Currency-conversion

React Currency-conversion, using API data.\
Branch: `currency`

### Counter & modal

Written standard counter and modal window.\
Branch: `counter & modal`

### Quiz

React questionnaire,using static data.\
Branch: `quiz`

### User-list

React questionnaire,using API data.\
Branch: `user-list`

### Photo-album

React questionnaire,using mockAPI data.\
Branch: `photo-album`
